#include <stdio.h>
#include <stdlib.h>
#include "minako.h"

int current_token;
int next_token;

int isToken(int in) {
    return current_token == in;
}

void eat() {
    current_token = next_token;
    next_token = yylex();
}

void isTokenAndEat(int in) {
    //不匹配就报错
    if (!isToken(in)) {
        fprintf(stderr, "ERROR: Syntaxfehler in Zeile(%d)\n", yylineno);
        exit(-1);
    }

    eat();
}

typedef void MetaSymbol(void);

MetaSymbol functiondefinition, functioncall, statementlist, block, statement, ifstatement, returnstatement, returnstatement_rks,
        printf_fuc, type, statassignment, assignment, expr, logic_op, simpexpr, simpexpr_rks, term, term_rks, factor, factor_rks;

//::= ( functiondefinition )* <EOF>
void program() {
    if (current_token == EOF) {
        return;
    } else {
        functiondefinition();
        program();
    }
}

//::= type <ID> "(" ")" "{" statementlist "}"
void functiondefinition() {
    type();
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
    isTokenAndEat('{');
    statementlist();
    isTokenAndEat('}');
}

//::= <ID> "(" ")"
void functioncall() {
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
}

// statementlist ::= block statementlist
void statementlist() {
    if (isToken('{') || isToken(KW_IF) ||
        isToken(KW_RETURN) ||
        isToken(KW_PRINTF) || isToken(ID)) {
        block();
        statementlist();
    }
}

//block ::= "{" statementlist "}"
//| statement
void block() {
    switch (current_token) {
        case '{':
            eat();
            statementlist();
            isTokenAndEat('}');
            break;
        default:
            statement();
            break;
    }

}

//statement             ::= ifstatement
//                          | returnstatement ";"
//                          | printf ";"
//                          | statassignment ";"
//                          | functioncall ";"
void statement() {
    switch (current_token) {
        case KW_IF:
            ifstatement();
            break;
        case KW_RETURN:
            returnstatement();
            isTokenAndEat(';');
            break;
        case KW_PRINTF:
            printf_fuc();
            isTokenAndEat(';');
            break;
            // stateassignment recognize by "=",
            // otherwise is funtioncall
        default:
            if (next_token == '=') {
                statassignment();
                isTokenAndEat(';');
            }
                // next_token has to be "(" to be correct syntax
            else {
                functioncall();
                isTokenAndEat(';');
            }
            break;
    }
}

//::= <KW_IF> "(" assignment ")" block
void ifstatement() {
    isTokenAndEat(KW_IF);
    isTokenAndEat('(');
    assignment();
    isTokenAndEat(')');
    block();
}

// split into right-rekursiv
void returnstatement() {
    isTokenAndEat(KW_RETURN);
    returnstatement_rks();
}

//rekursiv
void returnstatement_rks() {
    if (isToken(ID) || isToken('-') || isToken(CONST_INT) ||
        isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN) || isToken('(')) {
        assignment();
    }
}

//::= <KW_PRINTF> "(" assignment ")"
void printf_fuc() {
    isTokenAndEat(KW_PRINTF);
    isTokenAndEat('(');
    assignment();
    isTokenAndEat(')');
}

//::= <KW_BOOLEAN>
//    | <KW_FLOAT>
//    | <KW_INT>
//    | <KW_VOID>
void type() {
    if (isToken(KW_BOOLEAN) || isToken(KW_FLOAT) ||
        isToken(KW_INT) || isToken(KW_VOID)) {
        eat();
    }
}

//::= <ID> "=" assignment
void statassignment() {
    isTokenAndEat(ID);
    isTokenAndEat('=');
    assignment();
}

//::= ( ( <ID> "=" assignment ) | expr )
void assignment() {
    if (isToken(ID) && next_token == '=') {
        //吃两次
        eat();
        isTokenAndEat('=');
        assignment();
    } else {
        expr();
    }
}

//::= simpexpr ( ( "==" | "!=" | "<=" | ">=" | "<" | ">" ) simpexpr )?
void expr() {
    simpexpr();
    logic_op();
}

//save == and so on as logic_op
void logic_op() {
    if (isToken(EQ) || isToken(NEQ) || isToken(LEQ) ||
        isToken(GEQ) || isToken(LSS) || isToken(GRT)) {
        //消掉一个再检查simpexpr
        eat();
        simpexpr();
    }
}

//simpexpr::= ( "-" )? term ( ( "+" | "-" | "||" ) term )*
void simpexpr() {
//    occurs 0 oder random times
    if (isToken('-')) {
        eat();
    }

    term();
    simpexpr_rks();
}

void simpexpr_rks() {
    if (isToken('+') || isToken('-') || isToken(OR)) {
        eat();
        term();
        simpexpr_rks();
    }
}

//::= factor ( ( "*" | "/" | "&&" ) factor )*
void term() {
    factor();
    term_rks();
}

void term_rks() {
    if (isToken('*') || isToken('/') || isToken(AND)) {
        eat();
        factor();
        term_rks();
    }
}

//::= <CONST_INT>
//| <CONST_FLOAT>
//| <CONST_BOOLEAN>
//| functioncall
//| <ID>
//| "(" assignment ")"
void factor() {

    switch (current_token) {
        case (CONST_INT):
            eat();
            break;
        case CONST_FLOAT:
            eat();
            break;
        case CONST_BOOLEAN:
            eat();
            break;
        case '(':
            eat();
            assignment();
            isTokenAndEat(')');
            break;
        default:
            isTokenAndEat(ID);
            factor_rks();
            break;
    }
}

void factor_rks() {
    if (isToken('(')) {
        eat();
        isTokenAndEat(')');
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        yyin = stdin;
    } else {
        yyin = fopen(argv[1], "r");
        if (yyin == 0) {
            fprintf(
                    stderr,
                    "Fehler: Konnte Datei %s nicht zum lesen oeffnen.\n",
                    argv[1]
            );
            exit(-1);
        }
    }

    current_token = yylex();
    next_token = yylex();

    program();

    return 0;
}
